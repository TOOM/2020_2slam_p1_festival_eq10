<?php

namespace modele\metier;

/* 
 * Description d'une représentation
 * Une représentation se déroule sur un lieu à une date et une heure, avec un groupe
 * 
 */
class Representation {
    /**
     * code de la représentation : Rxx
     * @var string
     */
    private $id;
    
    /**
     * date en format : aaaa-mm-jj
     * @var string
     */
    private $date;
    
    /**
     * heure de début de représentation en format : hh:mm:ss
     * @var string
     */
    private $heureDebut;
    
    /**
     * heure de fin de représentation en format : hh:mm:ss
     * @var string
     */
    private $heureFin;
    
    /**
     * code du groupe : gxxx
     */
    private Groupe $unGroupe;
    
    /**
     * code du lieu : Lxx
     */
    private Lieu $unLieu;
    

    function __construct(string $id, string $date, string $heureDebut, string $heureFin, Groupe $leGroupe, Lieu $leLieu) {
        $this->id = $id;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
        $this->unGroupe = $leGroupe;
        $this->unLieu = $leLieu;
    }
    
    function getId(): string {
        return $this->id;
    }

    function getDate(): string {
        return $this->date;
    }

    function getHeureDebut(): string {
        return $this->heureDebut;
    }

    function getHeureFin(): string {
        return $this->heureFin;
    }

    function getUnGroupe() {
        return $this->unGroupe;
    }

    function getUnLieu() {
        return $this->unLieu;
    }

    function setId(string $id) {
        $this->id = $id;
    }

    function setDate(string $date) {
        $this->date = $date;
    }

    function setHeureDebut(string $heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin(string $heureFin) {
        $this->heureFin = $heureFin;
    }

    function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }

    function setUnLieu(Lieu $leLieu) {
        $this->unLieu = $leLieu;
    }


}
