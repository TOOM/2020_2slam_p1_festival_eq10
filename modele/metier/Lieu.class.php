<?php

namespace modele\metier;

/**
 * Description of Lieu
 *
 * @author estephan-mobe@jolsio.net
 */
class Lieu {
    // attributs
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $nom;
    /**
     * @var string
     */
    private $adresse;
    /**
     * @var integer
     */
    private $cptAccueil;
    
    // constructeur 
    function __construct($id, $nom, $adresse, $cptAccueil) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->cptAccueil = $cptAccueil;
    }
    
    // accesseurs et mutateurs
    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCptAccueil() {
        return $this->cptAccueil;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCptAccueil($cptAccueil) {
        $this->cptAccueil = $cptAccueil;
    }


}
