<?php

namespace modele\dao;

use modele\metier\Representation;
use PDOStatement;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author clément
 * @version 2020
 */

class RepresentationDAO{
    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $idRep = $enreg['ID'];
        $date = $enreg['DATE'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        $idGroupe = $enreg['IDGROUPE'];
        $idLieu = $enreg['IDLIEU'];
        
        // construire les objets Lieu et Groupe à partir de leur identifiant     
        $leGroupe = GroupeDAO::getOneById($idGroupe);
        $leLieu = LieuDAO::getOneById($idLieu);
        
        // instancier l'objet Attribution
        $objetMetier = new Representation($idRep, $date, $heureDebut, $heureFin, $leGroupe, $leLieu);

        return $objetMetier;
    }
    
    /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des representation d'un objet métier
     * @param Representation $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        
        $groupe = $objetMetier->getUnGroupe();
        $lieu = $objetMetier->getUnLieu();
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':date', $objetMetier->getDate());
        $stmt->bindValue(':heureDebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heureFin', $objetMetier->getHeureFin());
        $stmt->bindValue(':idGroupe', $groupe->getId());
        $stmt->bindValue(':idLieu', $lieu->getId());
        
    }
    
    /**
     * Retourne la liste de toutes les représentations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier une représentation et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche une représentation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la rerésentation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    public static function getAllByDate($date) {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation WHERE DATE = :date";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':date', $date);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier une représentation et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id, :date, :heureDebut, :heureFin, :idGroupe, :idLieu)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    public static function update($id, $date, $heureDebut, $heureFin, $idGroupe, $idLieu) {
        $ok = false;
        $RepresentationUpdate= self::getOneById($id);
        $RepresentationUpdate->setUnGroupe($idGroupe);
        $RepresentationUpdate->setUnLieu($idLieu);
        $RepresentationUpdate->setDate($date);
        $RepresentationUpdate->setHeureDebut($heureDebut);
        $RepresentationUpdate->setHeureFin($heureFin);

        $requete = "UPDATE  Representation SET ID=:id, IDGROUPE=:idGroupe, IDLIEU=:idLieu, DATE=:date,
           HEUREDEBUT=:heureDebut, HEUREFIN=:heureFin
           WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($RepresentationUpdate, $stmt);

        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
        
        
    }
    
    /**
     * Détruire un enregistrement de la table Representation d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    /**
     * Permet de vérifier s'il existe ou non une représentation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la représentation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
}
