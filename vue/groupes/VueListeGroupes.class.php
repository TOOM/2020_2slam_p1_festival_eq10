<?php
/**
 * Description Page de consultation de la liste des groupes
 * -> affiche Uun tableau constitué d'une ligne d'entête et d'une ligne par groupe
 * @author racin
 * @version 2018
 */
namespace vue\groupes;

use vue\VueGenerique;

class VueListeGroupes extends VueGenerique {
    
    /** @var array liste des groupes à afficher avec leur nombre d'atttributions */
    private $lesGroupesAvecNbAttributions;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille" >

            <tr class="enTeteTabNonQuad" >
                <td colspan="4" ><strong>Groupes</strong></td>
            </tr>
            <?php
            // Pour chaque groupe lu dans la base de données
            foreach ($this->lesGroupesAvecNbAttributions as $unGroupeAvecNbAttrib) {
                $unGroupe = $unGroupeAvecNbAttrib["groupe"];
                $id = $unGroupe->getId();
                $nom = $unGroupe->getNom();
                ?>
                <tr class="ligneTabNonQuad" >
                    <td width="52%" ><?= $nom ?></td>

                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=detail&id=<?= $id ?>" >
                            Voir détail</a>
                    </td>

                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=modifier&id=<?= $id ?>" >
                            Modifier
                        </a>
                    </td>

                    <?php
                    // S'il existe déjà des attributions pour le groupe, il faudra
                    // d'abord les supprimer avant de pouvoir supprimer le groupe
                   
                    ?>
                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=groupes&action=supprimerGroupe&id=<?= $id ?>" >
                            Supprimer
                        </a>
                    </td>
                    <?php
                    ?>
                    <td width="16%" >&nbsp; </td>
                    <?php
                    
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
        <br>
        <a href="index.php?controleur=groupes&action=creer" >
            Création d'un groupe</a >
        <?php
        include $this->getPied();
    }

    function setLesGroupesAvecNbAttributions($lesGroupesAvecNbAttributions) {
        $this->lesGroupesAvecNbAttributions = $lesGroupesAvecNbAttributions;
    }

}